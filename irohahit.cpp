#define _USE_MATH_DEFINES
#include "Jtol.h"
#include "argon2.h"
#include "openssl/sha.h"
#include <bits/stdc++.h>
using namespace Jtol;
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
typedef const unsigned long long cull;
// expansion macro for enum value definition
#define ENUM_VALUE(name, assign) name assign,

// expansion macro for enum to string conversion
#define ENUM_CASE(name, assign)                                                \
  case name:                                                                   \
    return #name;

// expansion macro for string to enum conversion
#define ENUM_STRCMP(name, assign)                                              \
  if (!strcmp(str, #name))                                                     \
    return name;

/// declare the access function and define enum values
#define DECLARE_ENUM(EnumType, ENUM_DEF)                                       \
  enum EnumType { ENUM_DEF(ENUM_VALUE) };                                      \
  const char *GetString(EnumType dummy);                                       \
  EnumType Get##EnumType##Value(const char *string);

/// define the access function names
#define DEFINE_ENUM(EnumType, ENUM_DEF)                                        \
  const char *GetString(EnumType value) {                                      \
    switch (value) {                                                           \
      ENUM_DEF(ENUM_CASE)                                                      \
    default:                                                                   \
      return ""; /* handle input error */                                      \
    }                                                                          \
  }                                                                            \
  EnumType Get##EnumType##Value(const char *str) {                             \
    ENUM_DEF(ENUM_STRCMP)                                                      \
    return (EnumType)0; /* handle input error */                               \
  }

#define SOME_ENUM(Status)                                                      \
  Status(kNULL, ) Status(kATTACK, ) Status(kATTACK_DYING, )                    \
      Status(kATTACK_DIE, ) Status(kATTACK_HPMAX, ) Status(kDEFENSE, ) Status( \
          kDEFENSE_DYING, ) Status(kDEFENSE_DIE, ) Status(kDEFENSE_HPMAX, )    \
          Status(kMAGIA, ) Status(kDAMAGE_UP_BAD, ) Status(kRESIST, ) Status(  \
              kACCEL, ) Status(kBLAST, ) Status(kCHARGE, ) Status(kDAMAGE, )   \
              Status(kDAMAGE_UP, ) Status(kDAMAGE_DOWN, ) Status(              \
                  kMP_GAIN, ) Status(kAUTO_HEAL, ) Status(kPROVOKE, )          \
                  Status(kCRITICAL, ) Status(kGUTS, ) Status(kCHARM, ) Status( \
                      kBLINDNESS, ) Status(kBURN, ) Status(kSTUN, )            \
                      Status(kAVOID, ) Status(kCURSE, ) Status(kRESTRAINT, )   \
                          Status(kPOISON, ) Status(kFOG, ) Status(kDARKNESS, ) \
                              Status(kPURSUE, ) Status(kCOUNTER, ) Status(     \
                                  kPROTECT, ) Status(kBAN_SKILL, )             \
                                  Status(kBAN_MAGIA, ) Status(kSKILL_QUICK, )  \
                                      Status(kDEFENSE_IGNORED, )               \
                                          Status(kMP_PLUS_WEAKED, )            \
                                              Status(kSURVIVE, )               \
                                                  Status(kIGNORE, )            \
                                                      Status(kSTATUS_END, )

DECLARE_ENUM(Status, SOME_ENUM);
DEFINE_ENUM(Status, SOME_ENUM);

random_device rng_dev;
mt19937 rng_mt(rng_dev());
int randint(int lb, int ub) {
  return uniform_int_distribution<int>(lb, ub)(rng_mt);
}
double randdouble(double lb, double ub) {
  return uniform_real_distribution<double>(lb, ub)(rng_mt);
}

uint64_t constexpr mix(char m, uint64_t s) {
  return ((s << 7) + ~(s >> 3)) + ~m;
}

uint64_t constexpr shash(const char *m) {
  return (*m) ? mix(*m, shash(m + 1)) : 0;
}
template <typename T> T null_or(const json &js, T k) {
  if (js.is_null())
    return k;
  return js;
}

struct PositionBuff {
  int atk = 0; // 0 ~ 10 ~ 15
  int def = 0;
};

struct StatusEffect {
  int value = 0;
  int turn = 0;
  int prob = 0;
  string target;
};

struct BattleChara {
  int id;
  vector<int> memories;
  int maxhp;
  int hp;
  int maxmp = 2000;
  int mp;
  map<Status, vector<StatusEffect>> status;
  PositionBuff pos_buf;
};

bool kIsMirror = true;

vector<int> five_start_char;
unordered_map<int, json> caraDb;
unordered_map<int, json> cardDb;
vector<int> ability_cards;
vector<int> skill_cards;
unordered_map<int, int> base_hp;
unordered_map<int, int> base_atk;
unordered_map<int, int> base_def;
void InitBaseHPATKDEF(int id){
  int hp=int(caraDb[id]["hp"]);
  int atk=int(caraDb[id]["attack"]);
  int def=int(caraDb[id]["defense"]);
  double h=1,a=1,d=1;
  switch (shash(string(caraDb[id]["growthType"]).c_str())) {
  case shash("BALANCE"):
    h=1,a=1,d=1;
    break;
  case shash("ATTACK"):
    h=0.98,a=1.03,d=0.97;
    break;
  case shash("DEFENSE"):
    h=0.97,a=0.98,d=1.05;
    break;
  case shash("HP"):
    h=1.04,a=0.97,d=0.98;
    break;
  case shash("ATKDEF"):
    h=0.99,a=1.02,d=1.01;
    break;
  case shash("ATKHP"):
    h=1.02,a=1.01,d=0.99;
    break;
  case shash("DEFHP"):
    h=1.01,a=0.99,d=1.02;
    break;
  }
  base_hp[id]=hp+hp*h*3;
  base_atk[id]=atk+atk*a*3;
  base_def[id]=def+def*d*3;
}
int GetBattlePoint(BattleChara &a){
  int hp = base_hp[a.id];
  int attack = int(caraDb[a.id]["attack"]) * 4;
  int defense = int(caraDb[a.id]["defense"]) * 4;
  for (int memory : a.memories) {
    hp += int(cardDb[memory]["hp"]) * 2.5;
    attack += int(cardDb[memory]["attack"]) * 2.5;
    defense += int(cardDb[memory]["defense"]) * 2.5;
  }
  return hp+attack+defense;
}
int GetMaxHP(BattleChara &a) {
  int hp = base_hp[a.id];
  assert(caraDb[a.id]["cardCustomize"]["bonusCode3"] == "HP");
  hp *= (1000.0 + int(caraDb[a.id]["cardCustomize"]["bonusNum3"])) / 1000;
  for (int memory : a.memories) {
    hp += int(cardDb[memory]["hp"]) * 2.5;
  }
  return hp;
}
int GetAttack(BattleChara &a) {
  int attack = base_atk[a.id];
  assert(caraDb[a.id]["cardCustomize"]["bonusCode1"] == "ATTACK");
  attack *= (1000.0 + int(caraDb[a.id]["cardCustomize"]["bonusNum1"])) / 1000;
  for (int memory : a.memories) {
    attack += int(cardDb[memory]["attack"]) * 2.5;
  }
  attack *= (100.0 + a.pos_buf.atk) / 100;
  int atk_buf = 0;
  for (auto buf : a.status[Status::kATTACK]) {
    atk_buf += buf.value;
  }
  if (a.hp * 5 < a.maxhp) {
    for (auto buf : a.status[Status::kATTACK_DYING]) {
      atk_buf += buf.value;
    }
  }
  if (a.hp == a.maxhp) {
    for (auto buf : a.status[Status::kATTACK_HPMAX]) {
      atk_buf += buf.value;
    }
  }
  atk_buf = max(atk_buf, 50);
  atk_buf = min(atk_buf, 1000);
  attack *= (1000.0 + atk_buf) / 1000;
  return attack;
}
int GetDefense(BattleChara &a) {
  int defense = base_def[a.id];
  assert(caraDb[a.id]["cardCustomize"]["bonusCode2"] == "DEFENSE");
  defense *= (1000.0 + int(caraDb[a.id]["cardCustomize"]["bonusNum2"])) / 1000;
  for (int memory : a.memories) {
    defense += int(cardDb[memory]["defense"]) * 2.5;
  }
  defense *= (100.0 + a.pos_buf.def) / 100;
  int def_buf = 0;
  for (auto buf : a.status[Status::kDEFENSE]) {
    def_buf += buf.value;
  }
  if (a.hp * 5 < a.maxhp) {
    for (auto buf : a.status[Status::kDEFENSE_DYING]) {
      def_buf += buf.value;
    }
  }
  if (a.hp == a.maxhp) {
    for (auto buf : a.status[Status::kDEFENSE_HPMAX]) {
      def_buf += buf.value;
    }
  }
  def_buf = max(def_buf, 50);
  def_buf = min(def_buf, 1000);
  defense *= (1000.0 + def_buf) / 1000;
  return defense;
}
void InitCardSkill(BattleChara &a) {
  for (int memory : a.memories) {
    auto &card = cardDb[memory]["pieceSkill2"];
    if (card["type"] != "ABILITY")
      continue;
    for (int i = 1;; i++) {
      auto &art = card["art" + to_string(i)];
      if (art.is_null())
        break;
      Status status;
      auto calc_status = [&](int eff) {
        if (status == Status::kNULL) {
          cerr << memory << " Unknown effectCode: " << string(art["effectCode"])
               << endl;
        }
        a.status[status].push_back(
            StatusEffect{.value = eff * null_or(art["effectValue"], 0),
                         .turn = 99999,
                         .prob = null_or(art["probability"], 0),
                         .target = null_or(art["targetId"], string(""))});
      };
      switch (shash(string(art["verbCode"]).c_str())) {
      case shash("BUFF"):
        status = GetStatusValue(("k" + string(art["effectCode"])).c_str());
        calc_status(1);
        break;
      case shash("BUFF_HPMAX"):
        status = GetStatusValue(
            ("k" + string(art["effectCode"]) + "_HPMAX").c_str());
        calc_status(1);
        break;
      case shash("BUFF_DYING"):
        status = GetStatusValue(
            ("k" + string(art["effectCode"]) + "_DYING").c_str());
        calc_status(1);
        break;
      case shash("BUFF_DIE"):
        status =
            GetStatusValue(("k" + string(art["effectCode"]) + "_DIE").c_str());
        calc_status(1);
        break;
      case shash("DEBUFF"):
        status = GetStatusValue(("k" + string(art["effectCode"])).c_str());
        calc_status(-1);
        break;
      case shash("CONDITION_GOOD"):
        status = GetStatusValue(("k" + string(art["effectCode"])).c_str());
        calc_status(1);
        break;
      case shash("INITIAL"):
        if (art["effectCode"] == "MP") {
          a.mp += int(art["effectValue"]) * a.maxmp / 1000;
        } else {
          cerr << memory << " Unknown effectCode: " << string(art["effectCode"])
               << endl;
        }
        break;
      case shash("ENCHANT"):
        break;
      case shash("IGNORE"):
        status = GetStatusValue(("k" + string(art["effectCode"])).c_str());
        if (status == Status::kNULL) {
          cerr << memory << " Unknown effectCode: " << string(art["effectCode"])
               << endl;
        }
        a.status[Status::kIGNORE].push_back(
            StatusEffect{.value = int(status),
                         .turn = 99999,
                         .prob = null_or(art["probability"], 0),
                         .target = null_or(art["targetId"], string(""))});
        break;
      case shash("OTHER"):
        break;
      default:
        cerr << memory << " Unknown verbCode: " << string(art["verbCode"])
             << endl;
      }
    }
  }
}

int GetStatusTotal(BattleChara &a, Status status) {
  int result = 0;
  for (auto buf : a.status[status]) {
    result += buf.value;
  }
  return result;
}

int GetStatusProb(BattleChara &a, Status status) {
  int result = 0;
  for (auto buf : a.status[status]) {
    result = max(result, buf.prob);
  }
  return result;
}

int GetIgnoreProb(BattleChara &a, Status status) {
  int result = 0;
  for (auto buf : a.status[Status::kIGNORE]) {
    if (buf.value == int(status))
      result = max(result, buf.prob);
  }
  return result;
}

int GetStatusMax(BattleChara &a, Status status) {
  int result = 0;
  for (auto buf : a.status[status]) {
    result = max(result, buf.value);
  }
  return result;
}

bool IsInBadStatus(BattleChara &a) {
  static vector<Status> bad_status = {
      Status::kCHARM,    Status::kBLINDNESS, Status::kBURN,     Status::kSTUN,
      Status::kCURSE,    Status::kRESTRAINT, Status::kPOISON,   Status::kFOG,
      Status::kDARKNESS, Status::kBAN_MAGIA, Status::kBAN_SKILL};
  for (auto &status : bad_status) {
    if (a.status[status].size())
      return true;
  }
  return false;
}

void SetUpChara(BattleChara &a) {
  a.status.clear();
  InitCardSkill(a);
  a.maxhp = GetMaxHP(a);
  a.hp = a.maxhp;
  a.maxmp = 2000;
  a.mp = 0;
}

enum Disk { kAccel, kBlastH, kBlastV, kCharge, kMagia, kDoppel, kNormal };

// disk_pos start from 0
void Hit(BattleChara &a, BattleChara &b, Disk disk, int disk_pos, bool is_pc,
         bool is_abc_combo, int &charge_cnt) { // A hit B
  static double c_a_table[21] = {1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6,
                                 1.7, 1.8, 1.9, 2.0, 2.1, 2.2, 2.3,
                                 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0};
  static double c_b_table[21] = {1.0, 1.4, 1.7, 2.0, 2.3, 2.5, 2.7,
                                 2.9, 3.1, 3.3, 3.5, 3.7, 3.9, 4.1,
                                 4.3, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0};
  int a_atk = GetAttack(a);
  int b_def = GetDefense(b);
  int base_dmg = max((a_atk - b_def / 3), 500);
  int final_damage = 0;
  if (disk == Disk::kAccel || disk == Disk::kBlastH || disk == Disk::kBlastV ||
      disk == Disk::kCharge) {
    double disk_arg = 0;
    if (disk == Disk::kAccel) {
      if (is_pc)
        disk_arg = 1.2;
      else
        disk_arg = 1.0;
    } else if (disk == Disk::kBlastH || disk == Disk::kBlastV) {
      disk_arg = 0.6;
      if (is_pc)
        disk_arg += 0.3;
      if (is_abc_combo)
        disk_arg += 0.3;
      if (kIsMirror)
        disk_arg += 0.1;
    } else if (disk == Disk::kCharge) {
      if (is_pc)
        disk_arg = 1.2;
      else
        disk_arg = 0.8;
    }
    double awake = 1.0;
    if (disk == Disk::kAccel) {
      assert(caraDb[a.id]["cardCustomize"]["bonusCode4"] == "ACCEL");
      awake += double(caraDb[a.id]["cardCustomize"]["bonusNum4"]) / 1000;
    } else if (disk == Disk::kBlastH || disk == Disk::kBlastV) {
      assert(caraDb[a.id]["cardCustomize"]["bonusCode5"] == "BLAST");
      awake += double(caraDb[a.id]["cardCustomize"]["bonusNum5"]) / 1000;
    } else if (disk == Disk::kCharge) {
      assert(caraDb[a.id]["cardCustomize"]["bonusCode6"] == "CHARGE");
      awake += double(caraDb[a.id]["cardCustomize"]["bonusNum6"]) / 1000;
    }
    double b_disk_pos_arg = 1.0;
    if (disk == Disk::kBlastH || disk == Disk::kBlastV) {
      b_disk_pos_arg += disk_pos * 0.1;
    }
    double c_disk_arg = 1.0;
    if (charge_cnt && disk == Disk::kAccel) {
      c_disk_arg = c_a_table[charge_cnt] *
                   (1000 + GetStatusTotal(a, Status::kCHARGE)) / 1000;
      if (kIsMirror)
        c_disk_arg *= 0.9;
      c_disk_arg = min(c_disk_arg, 5.5);
      charge_cnt = 0;
    } else if (charge_cnt && (disk == Disk::kBlastH || disk == Disk::kBlastV)) {
      c_disk_arg = c_b_table[charge_cnt] *
                   (1000 + GetStatusTotal(a, Status::kCHARGE)) / 1000;
      if (kIsMirror)
        c_disk_arg *= 0.9;
      c_disk_arg = min(c_disk_arg, 5.5);
      charge_cnt = 0;
    }
    double aliment = 1.0;
    if (caraDb[a.id]["attributeId"] == "LIGHT" &&
        caraDb[b.id]["attributeId"] == "DARK") {
      aliment = 1.5;
    } else if (caraDb[a.id]["attributeId"] == "DARK" &&
               caraDb[b.id]["attributeId"] == "LIGHT") {
      aliment = 1.5;
    } else if (caraDb[a.id]["attributeId"] == "FIRE" &&
               caraDb[b.id]["attributeId"] == "TIMBER") {
      aliment = 1.5;
    } else if (caraDb[a.id]["attributeId"] == "WATER" &&
               caraDb[b.id]["attributeId"] == "FIRE") {
      aliment = 1.5;
    } else if (caraDb[a.id]["attributeId"] == "TIMBER" &&
               caraDb[b.id]["attributeId"] == "WATER") {
      aliment = 1.5;
    } else if (caraDb[a.id]["attributeId"] == "TIMBER" &&
               caraDb[b.id]["attributeId"] == "FIRE") {
      aliment = 0.5;
    } else if (caraDb[a.id]["attributeId"] == "FIRE" &&
               caraDb[b.id]["attributeId"] == "WATER") {
      aliment = 0.5;
    } else if (caraDb[a.id]["attributeId"] == "WATER" &&
               caraDb[b.id]["attributeId"] == "TIMBER") {
      aliment = 0.5;
    }
    int bonus = 0;
    bonus += GetStatusTotal(a, Status::kDAMAGE);
    bonus += GetStatusTotal(a, Status::kDAMAGE_UP);

    if (randint(0, 999) >= GetIgnoreProb(a, Status::kDAMAGE_DOWN)) {
      bonus -= GetStatusTotal(b, Status::kDAMAGE_DOWN);
    }
    if (disk == Disk::kBlastH || disk == Disk::kBlastV) {
      bonus += GetStatusTotal(a, Status::kBLAST);
    }
    if (IsInBadStatus(b)) {
      bonus += GetStatusTotal(a, Status::kDAMAGE_UP_BAD);
    }
    double bonus_dmg = (1000.0 + bonus) / 1000;
    bonus_dmg = max(bonus_dmg, 0.3);
    bonus_dmg = min(bonus_dmg, 3.0);
    if (randint(0, 999) < GetStatusProb(a, Status::kCRITICAL)) {
      if (bonus_dmg <= 1)
        bonus_dmg *= 2;
      else
        bonus_dmg += 1;
    }
    final_damage = base_dmg * disk_arg * awake * b_disk_pos_arg * c_disk_arg *
                   aliment * bonus_dmg;
    if (kIsMirror) {
      final_damage *= 0.7;
    }
  } else {
    // TODO;
  }
  final_damage = max(final_damage, 250);
  final_damage *= randdouble(0.95, 1.05);
  b.hp -= final_damage;
  if (b.hp < 0)
    b.hp = 0;
}
void LoadData() {
  json charaCard =
      json::parse(FileToStr(path("collection") / path("charaCard.json")));
  json memoria =
      json::parse(FileToStr(path("collection") / path("memoria.json")));
  auto &cardList = charaCard["cardList"];
  cout << "Chara Size: " << cardList.size() << endl;
  for (auto &[key, value] : cardList.items()) {
    caraDb[StrToInt(key)] = value;
    InitBaseHPATKDEF(StrToInt(key));
    if (key.back() == '5') {
      five_start_char.push_back(StrToInt(key));
    }
  }
  cout << "Memoria Size: " << memoria.size() << endl;
  for (auto &[key, value] : memoria.items()) {
    cardDb[StrToInt(key)] = value;
    if(value["rank"]!="RANK_4")continue;
    if(value["pieceType"]=="ABILITY"){
      ability_cards.push_back(StrToInt(key));
    }else if(value["pieceType"]=="SKILL"){
      if(int(value["pieceSkill2"]["intervalTurn"])>5)continue;
      if(StrToInt(key)==1274)continue;
      if(StrToInt(key)==1146)continue;
      if(StrToInt(key)==1200)continue;
      if(StrToInt(key)==1218)continue;
      if(StrToInt(key)==1229)continue;
      if(StrToInt(key)==1229)continue;
      if(StrToInt(key)==1215)continue;
      if(StrToInt(key)==1390)continue;
      if(StrToInt(key)==1224)continue;
      if(StrToInt(key)==1244)continue;
      skill_cards.push_back(StrToInt(key));
    }else{
      cerr<<"pieceType???"<<endl;
    }
  }
}
