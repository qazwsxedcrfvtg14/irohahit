#include "irohahit.cpp"

int main() {
  LoadData();
  for(int &x:ability_cards){
    if(x==1225){
      swap(x,ability_cards.back());
      ability_cards.pop_back();
      break;
    }
  }
  kIsMirror = true;
  vector<BattleChara> ve;
  //ve.push_back(BattleChara{.id=30265});
  //ve.push_back(BattleChara{.id=30435});
  //ve.push_back(BattleChara{.id=30165});
  ve.push_back(BattleChara{.id=21015});
  ve.push_back(BattleChara{.id=13015});
  vector<int>target_hp;
  //target_hp.push_back(36778);
  //target_hp.push_back(37117);
  //target_hp.push_back(38281);
  //int tot=183167;
  target_hp.push_back(38920);
  target_hp.push_back(35849);
  int tot=120331;
  ability_cards.clear();
  ability_cards.push_back(1386);
  //ability_cards.push_back(1383);
  ability_cards.push_back(1367);
  ability_cards.push_back(1365);
  //ability_cards.push_back(1333);
  //ability_cards.push_back(1328);
  ability_cards.push_back(1316);
  ability_cards.push_back(1302);
  //ability_cards.push_back(1283);
  //ability_cards.push_back(1269);
  ability_cards.push_back(1267);
  ability_cards.push_back(1260);
  ability_cards.push_back(1255);
  //ability_cards.push_back(1252);
  ability_cards.push_back(1246);
  //ability_cards.push_back(1239);
  ability_cards.push_back(1226);
  ability_cards.push_back(1219);
  ability_cards.push_back(1209);
  ability_cards.push_back(1203);
  //ability_cards.push_back(1196);
  ability_cards.push_back(1190);
  ability_cards.push_back(1161);
  ability_cards.push_back(1144);
  ability_cards.push_back(1130);
  ability_cards.push_back(1122);
  ability_cards.push_back(1117);
  //ability_cards.push_back(1069);
  ability_cards.push_back(1041);
  skill_cards.push_back(1155);
  unordered_map<int,int>mp;
  vector<vector<vector<int>>> ans(ve.size());
  function<void(int,int)> F=[&](int x,int y){
    if(y==2&&ve[x].memories[0]>=ve[x].memories[1])return;
    if(y==2&&ve[x].memories[0]==1203&&ve[x].memories[1]==1209)return;
    if(y==2&&ve[x].memories[0]==1190&&ve[x].memories[1]==1209)return;
    if(y==4&&ve[x].memories[2]>=ve[x].memories[3])return;
    int total_point=0;
    for(auto &a:ve)
      total_point+=GetBattlePoint(a);
    mp.clear();
    for(auto &a:ve)
      for(int b:a.memories)
        mp[b]++;
    if(mp[1134]>1)return;
    if(mp[1280]>1)return;
    if(mp[1252]>1)return;
    if(mp[1386]>1)return;
    if(mp[1144]>1)return;
    if(mp[1267]>1)return;
    if(total_point>tot)return;
    if(y==4){
      int hp = GetMaxHP(ve[x]);
      if(hp!=target_hp[x])return;
      if(x+1==ve.size()){
        for(int i=0;i<ve.size();i++)
          ans[i].push_back(ve[i].memories);
        for(auto &a:ve){
          for(int b:a.memories){
            printf("%d ",b);
          }
          puts("");
        }
        puts("------------------------------");
        return;
      }
      F(x+1,0);
      return;
    }
    int hp = GetMaxHP(ve[x]);
    if(hp>target_hp[x])return;
    if(hp+2300*(4-y)<target_hp[x])return;
    if(y<2){
      for(int z:ability_cards){
        ve[x].memories.push_back(z);
        F(x,y+1);
        ve[x].memories.pop_back();
      }
    }
    else{
      for(int z:skill_cards){
        ve[x].memories.push_back(z);
        F(x,y+1);
        ve[x].memories.pop_back();
      }
    }
    return;
  };
  F(0,0);
  for(int i=0;i<ve.size();i++){
    sort(ans[i].begin(),ans[i].end(),[](const vector<int>&a,const vector<int>&b)->bool{
      for(int j=0;j<a.size();j++)
        if(a[j]<b[j])return true;
        else if(a[j]>b[j])return false;
      return false;
    });
    ans[i].resize(unique(ans[i].begin(),ans[i].end(),[](const vector<int>&a,const vector<int>&b)->bool{
      for(int j=0;j<a.size();j++)
        if(a[j]!=b[j])return false;
      return true;
    })-ans[i].begin());
    for(auto &a:ans[i]){
      for(int b:a){
        printf("%d ",b);
      }
      puts("");
    }
    puts("~~~~~~~~~~~~~");
  }
  return 0;
  BattleChara iroha{.id = 13015};
  iroha.memories.push_back(1255);
  iroha.memories.push_back(1219);
  iroha.memories.push_back(1167);
  iroha.memories.push_back(1151);
  iroha.pos_buf = PositionBuff{.atk = 10, .def = 10};
  BattleChara madoka{.id = 20015};
  madoka.memories.push_back(1255);
  // madoka.memories.push_back(1219);
  madoka.memories.push_back(1365);
  madoka.memories.push_back(1121);
  madoka.memories.push_back(1151);
  madoka.pos_buf = PositionBuff{.atk = 10, .def = 10};
  cout << GetMaxHP(iroha) << endl;
  cout << GetAttack(iroha) << endl;
  cout << GetDefense(iroha) << endl;
  cout << GetMaxHP(madoka) << endl;
  cout << GetAttack(madoka) << endl;
  cout << GetDefense(madoka) << endl;

  while (true) {
    SetUpChara(iroha);
    SetUpChara(madoka);
    int charge = 0;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    cout << "madoka.hp: " << madoka.hp << endl;
    Hit(iroha, madoka, Disk::kAccel, 0, true, true, charge);
    cout << "madoka.hp: " << madoka.hp << endl;
    Hit(iroha, madoka, Disk::kCharge, 1, true, true, charge);
    cout << "madoka.hp: " << madoka.hp << endl;
    Hit(iroha, madoka, Disk::kBlastH, 2, true, true, charge);
    cout << "madoka.hp: " << madoka.hp << endl;
    string s;
    cin >> s;
  }
  return 0;
}
